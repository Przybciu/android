package pl.polsl.przybylski.app1.Figures;

import pl.polsl.przybylski.app1.R;

public class Square extends Figure {
    private final float side;
    private float diagonal;

    public Square(float side) {
        super("SQUARE");
        this.side = side;
        calculateDiagonal();
    }

    @Override
    public float getSurfaceArea() {
        return side * side;
    }

    private void calculateDiagonal() {
        diagonal = (float) (side * Math.sqrt(2));
    }

    @Override
    public void printFigureInfo() {
        System.out.println(getBasicFigureInfo() + " and diagonal = " + format.format(diagonal));
    }

    @Override
    public String getFeatureInfo() {
        return "d= " + format.format(diagonal);
    }

    @Override
    public float getFeature() {
        return diagonal;
    }

    @Override
    public int getDrawable() {
        return R.drawable.square;
    }

    public float getSide() {
        return side;
    }

    @Override
    public float getValue() {
        return getSide();
    }
}
