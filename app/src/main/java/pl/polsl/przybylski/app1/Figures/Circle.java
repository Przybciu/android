package pl.polsl.przybylski.app1.Figures;

import pl.polsl.przybylski.app1.R;

public class Circle extends Figure {
    private final float radius;
    private float diameter;

    public Circle(float radius) {
        super("CIRCLE");
        this.radius = radius;
        calculateDiameter();
    }

    @Override
    public float getSurfaceArea() {
        return (float) (Math.PI * radius * radius);
    }

    private void calculateDiameter() {
        diameter = 2 * radius;
    }

    @Override
    public void printFigureInfo() {
        System.out.println(getBasicFigureInfo() + " and diameter = " + format.format(diameter));
    }

    @Override
    public String getFeatureInfo() {
        return "d= " + format.format(diameter);
    }

    @Override
    public float getFeature() {
        return diameter;
    }

    @Override
    public int getDrawable() {
        return R.drawable.circle;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public float getValue() {
        return getRadius();
    }
}
