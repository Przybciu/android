package pl.polsl.przybylski.app1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import pl.polsl.przybylski.app1.Figures.Circle;
import pl.polsl.przybylski.app1.Figures.Figure;
import pl.polsl.przybylski.app1.Figures.Square;
import pl.polsl.przybylski.app1.Figures.Triangle;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_MESSAGE_MIN = "EXTRA_MESSAGE_MIN";
    public static final String EXTRA_MESSAGE_MAX = "EXTRA_MESSAGE_MAX";
    public static final String EXTRA_MESSAGE_TYPE_COUNT = "EXTRA_MESSAGE_TYPE_COUNT";
    public static final String EXTRA_MESSAGE_AREA = "EXTRA_MESSAGE_AREA";
    public static final String EXTRA_MESSAGE_FEATURE = "EXTRA_MESSAGE_FEATURE";
    public static final String EXTRA_MESSAGE_VALUE = "EXTRA_MESSAGE_VALUE";
    public static final String EXTRA_MESSAGE_TYPE = "EXTRA_MESSAGE_TYPE";
    private static final boolean FIELD_CLICK = false;
    private static final boolean FEATURE_CLICK = false;
    private static int SIZE = 50;
    private static int MIN = 0;
    private static int MAX = 1;
    private static int SORT_PARAMETER = 0;
    private static int SORT_DIRECTION = 0;
    private static boolean FIGURE_CLICK = false;
    private final DecimalFormat format = new DecimalFormat("#.###");
    private List<Figure> figures;
    public static List<Figure> figureList;
    private final Random generator = new Random();
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SORT_PARAMETER = 0;
        SORT_DIRECTION = 0;
        layout = findViewById(R.id.layout);
        figures = new ArrayList<>();

        Intent intent = getIntent();
        String message = intent.getStringExtra(SettingsActivity.EXTRA_MESSAGE);
        if (message != null && !message.isEmpty()) {
            SIZE = Integer.valueOf(message);
        }
        String messageMin = intent.getStringExtra(SettingsActivity.EXTRA_MESSAGE_MIN);
        if (messageMin != null && !messageMin.isEmpty()) {
            MIN = Integer.valueOf(messageMin);
        }
        String messageMax = intent.getStringExtra(SettingsActivity.EXTRA_MESSAGE_MAX);
        if (messageMax != null && !messageMax.isEmpty()) {
            MAX = Integer.valueOf(messageMax);
        }
        Figure figure = null;
        String messageType = intent.getStringExtra(EditActivity.EXTRA_MESSAGE_TYPE);
        String messageValue = intent.getStringExtra(EditActivity.EXTRA_MESSAGE_VALUE);
        if(messageType != null && !messageType.isEmpty() && messageValue != null && !messageValue.isEmpty()) {
            figures = EditActivity.figures;
            switch (messageType) {
                case "Kwadrat": {
                    figure = new Square(Float.parseFloat(messageValue));
                    break;
                }
                case "Trójkąt": {
                    figure = new Triangle(Float.parseFloat(messageValue));
                    break;
                }
                default: {
                    figure = new Circle(Float.parseFloat(messageValue));
                    break;
                }
            }
            View row = getLayoutInflater().inflate(R.layout.row, null);
            ImageView image = row.findViewById(R.id.image);
            TextView text = row.findViewById(R.id.text1);
            TextView textTwo = row.findViewById(R.id.text2);
            image.setImageResource(figure.getDrawable());
            text.setText(figure.getSurfaceAreaFormatted());
            textTwo.setText(figure.getFeatureInfo());
            row.setOnClickListener(v -> openPopupMenu(v));
            layout.addView(row);
            figures.add(figure);
        }
        if(figure == null) {
            fillFiguresList();
        }
        displayFigures();
    }

    private void displayFigures() {
        layout.removeAllViews();
        if (SORT_PARAMETER == 1) {
            Collections.sort(figures, Comparator.comparing(figure -> figure.getName()));
        } else if (SORT_PARAMETER == 2) {
            Collections.sort(figures, Comparator.comparing(figure -> figure.getSurfaceArea()));
        } else if (SORT_PARAMETER == 3) {
            Collections.sort(figures, Comparator.comparing(figure -> figure.getFeature()));
        }
        if (SORT_PARAMETER != 0 && SORT_DIRECTION == 1) {
            Collections.reverse(figures);
        }
        figures.forEach(figure -> {
            View row = getLayoutInflater().inflate(R.layout.row, null);
            ImageView image = row.findViewById(R.id.image);
            TextView text = row.findViewById(R.id.text1);
            TextView textTwo = row.findViewById(R.id.text2);
            image.setImageResource(figure.getDrawable());
            text.setText(figure.getSurfaceAreaFormatted());
            textTwo.setText(figure.getFeatureInfo());
            row.setOnClickListener(v -> openPopupMenu(v));
            layout.addView(row);
        });
    }

    private void fillFiguresList() {
        figures.clear();
        for (int i = 0; i < SIZE; i++) {
            figures.add(createFigure(generator.nextInt(3), MIN, MAX));
        }
    }

    private Figure createFigure(int nextInt, int min, int max) {
        float randomNumber = (float) (min + Math.random() * (max - min));
        if (randomNumber == 0) {
            randomNumber = max - min;
        } else if (randomNumber < 0.5 && max == 1) {
            randomNumber += 0.25;
        }
        if (nextInt == 0) {
            return new Square(randomNumber);
        } else if (nextInt == 1) {
            return new Triangle(randomNumber);
        } else {
            return new Circle(randomNumber);
        }
    }

    public void openSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        String message = String.valueOf(SIZE);
        intent.putExtra(EXTRA_MESSAGE, message);
        String messageMin = String.valueOf(MIN);
        intent.putExtra(EXTRA_MESSAGE_MIN, messageMin);
        String messageMax = String.valueOf(MAX);
        intent.putExtra(EXTRA_MESSAGE_MAX, messageMax);
        startActivity(intent);
    }

    public void openStatisticsActivity(View view) {
        Intent intent = new Intent(this, StatisticsActivity.class);
        String message = getFigureTypeCount();
        intent.putExtra(EXTRA_MESSAGE_TYPE_COUNT, message);
        String messageArea = getFigureAreaSum();
        intent.putExtra(EXTRA_MESSAGE_AREA, messageArea);
        String messageFeature = getFigureFeatureSum();
        intent.putExtra(EXTRA_MESSAGE_FEATURE, messageFeature);
        startActivity(intent);
    }

    public void openInfoActivity(View view) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

    private String getFigureFeatureSum() {
        float triangleSum = 0;
        float squareSum = 0;
        float circleSum = 0;

        for (Figure figure : figures) {
            if (figure instanceof Triangle) {
                triangleSum += figure.getFeature();
            } else if (figure instanceof Square) {
                squareSum += figure.getFeature();
            } else {
                circleSum += figure.getFeature();
            }
        }
        return format.format(triangleSum) + "-" + format.format(squareSum) + "-" + format.format(circleSum);
    }

    private String getFigureAreaSum() {
        float triangleSum = 0;
        float squareSum = 0;
        float circleSum = 0;

        for (Figure figure : figures) {
            if (figure instanceof Triangle) {
                triangleSum += figure.getSurfaceArea();
            } else if (figure instanceof Square) {
                squareSum += figure.getSurfaceArea();
            } else {
                circleSum += figure.getSurfaceArea();
            }
        }
        return format.format(triangleSum) + "-" + format.format(squareSum) + "-" + format.format(circleSum);
    }

    private String getFigureTypeCount() {
        int triangleSum = 0;
        int squareSum = 0;
        int circleSum = 0;

        for (Figure figure : figures) {
            if (figure instanceof Triangle) {
                triangleSum += 1;
            } else if (figure instanceof Square) {
                squareSum += 1;
            } else {
                circleSum += 1;
            }
        }
        return triangleSum + "-" + squareSum + "-" + circleSum;
    }

    public void sortByFigure(View view) {
        setDirection();
        SORT_PARAMETER = 1;
        displayFigures();
    }

    public void sortByFieldArea(View view) {
        setDirection();
        SORT_PARAMETER = 2;
        displayFigures();
    }

    public void sortByFeature(View view) {
        setDirection();
        SORT_PARAMETER = 3;
        displayFigures();
    }

    private void setDirection() {
        if (FIGURE_CLICK == false) {
            SORT_DIRECTION = 0;
            FIGURE_CLICK = true;
        } else {
            SORT_DIRECTION = 1;
            FIGURE_CLICK = false;
        }
    }

    public void openPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(MainActivity.this, view);
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        TextView text = view.findViewById(R.id.text1);
        TextView textTwo = view.findViewById(R.id.text2);

        String text1 = text.getText().toString();
        String text2 = textTwo.getText().toString();

        popup.setOnMenuItemClickListener(item -> {
            CharSequence title = item.getTitle();
            if ("usuń".equals(title)) {
                Optional<Figure> collect = figures.stream().filter(f -> f.getSurfaceAreaFormatted().equals(text1) && f.getFeatureInfo().equals(text2)).findFirst();
                if (collect.isPresent()) {
                    figures.remove(collect.get());
                    layout.removeView(view);
                }
            } else if ("duplikuj".equals(title)) {
                Optional<Figure> collect = figures.stream().filter(f -> f.getSurfaceAreaFormatted().equals(text1) && f.getFeatureInfo().equals(text2)).findFirst();
                if (collect.isPresent()) {
                    figures.add(collect.get());
                    View row = getLayoutInflater().inflate(R.layout.row, null);
                    ImageView image = row.findViewById(R.id.image);
                    TextView text_2 = row.findViewById(R.id.text1);
                    TextView textTwo_2 = row.findViewById(R.id.text2);
                    image.setImageResource(collect.get().getDrawable());
                    text_2.setText(collect.get().getSurfaceAreaFormatted());
                    textTwo_2.setText(collect.get().getFeatureInfo());
                    row.setOnClickListener(v -> openPopupMenu(v));
                    layout.addView(row, figures.indexOf(collect.get()) + 1);
                }
            } else if ("dodaj losowy".equals(title)) {
                Figure figure = createFigure(generator.nextInt(3), MIN, MAX);
                Optional<Figure> collect = figures.stream().filter(f -> f.getSurfaceAreaFormatted().equals(text1) && f.getFeatureInfo().equals(text2)).findFirst();
                figures.add(figure);
                View row = getLayoutInflater().inflate(R.layout.row, null);
                ImageView image = row.findViewById(R.id.image);
                TextView text_2 = row.findViewById(R.id.text1);
                TextView textTwo_2 = row.findViewById(R.id.text2);
                image.setImageResource(figure.getDrawable());
                text_2.setText(figure.getSurfaceAreaFormatted());
                textTwo_2.setText(figure.getFeatureInfo());
                row.setOnClickListener(v -> openPopupMenu(v));
                if (collect.isPresent()) {
                    layout.addView(row, figures.indexOf(collect.get()) + 1);
                } else {
                    layout.addView(row, figures.indexOf(figures.size() - 1));
                }
            } else if ("edytuj".equals(title)) {
                figureList = figures;
                Optional<Figure> collect = figures.stream().filter(f -> f.getSurfaceAreaFormatted().equals(text1) && f.getFeatureInfo().equals(text2)).findFirst();
                if (collect.isPresent()) {
                    Figure figure = collect.get();
                    Intent intent = new Intent(this, EditActivity.class);
                    String message;
                    if (figure instanceof Triangle) {
                        message = "Trójkąt";
                    } else if (figure instanceof Square) {
                        message = "Kwadrat";
                    } else {
                        message = "Koło";
                    }
                    figures.remove(figure);
                    intent.putExtra(EXTRA_MESSAGE_VALUE, String.valueOf(figure.getValue()));
                    intent.putExtra(EXTRA_MESSAGE_TYPE, message);
                    startActivity(intent);
                }
            }
            return true;
        });
        popup.show();
    }

    public void restartFigures(View view) {
        figures.clear();
        fillFiguresList();
        displayFigures();
    }
}