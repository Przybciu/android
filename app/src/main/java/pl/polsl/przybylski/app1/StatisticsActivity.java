package pl.polsl.przybylski.app1;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class StatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        TextView editCircleAmount = findViewById(R.id.editCircleAmount);
        TextView editSquareAmount = findViewById(R.id.editSquareAmount);
        TextView editTriangleAmount = findViewById(R.id.editTriangleAmount);

        TextView editCircleAreaSum = findViewById(R.id.editCircleAreaSum);
        TextView editSquareAreaSum = findViewById(R.id.editSquareAreaSum);
        TextView editTriangleAreaSum = findViewById(R.id.editTriangleAreaSum);

        TextView editCircleFeatureSum = findViewById(R.id.editCircleFeatureSum);
        TextView editSquareFeatureSum = findViewById(R.id.editSquareFeatureSum);
        TextView editTriangleFeatureSum = findViewById(R.id.editTriangleFeatureSum);

        Intent intent = getIntent();
        String messageType = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_TYPE_COUNT);
        String messageFeature = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_FEATURE);
        String messageArea = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_AREA);
        String[] typesCounts = messageType.split("-", 3);
        String[] featureCounts = messageFeature.split("-", 3);
        String[] areaCounts = messageArea.split("-", 3);

        editCircleAmount.setText(typesCounts[2]);
        editSquareAmount.setText(typesCounts[1]);
        editTriangleAmount.setText(typesCounts[0]);

        editCircleAreaSum.setText(featureCounts[2]);
        editSquareAreaSum.setText(featureCounts[1]);
        editTriangleAreaSum.setText(featureCounts[0]);

        editCircleFeatureSum.setText(areaCounts[2]);
        editSquareFeatureSum.setText(areaCounts[1]);
        editTriangleFeatureSum.setText(areaCounts[0]);
    }
}