package pl.polsl.przybylski.app1.Figures;

import pl.polsl.przybylski.app1.R;

public class Triangle extends Figure {
    private final float side;
    private float height;

    public Triangle(float side) {
        super("TRIANGLE");
        this.side = side;
        calculateHeight();
    }

    @Override
    public float getSurfaceArea() {
        return (float) ((side * side * Math.sqrt(3)) / 4);
    }

    private void calculateHeight() {
        height = (float) (side * Math.sqrt(3)) / 2;
    }

    @Override
    public void printFigureInfo() {
        System.out.println(getBasicFigureInfo() + " and height = " + format.format(height));
    }

    @Override
    public String getFeatureInfo() {
        return "h= " + format.format(height);
    }

    @Override
    public float getFeature() {
        return height;
    }

    @Override
    public int getDrawable() {
        return R.drawable.triangle;
    }

    public float getSide() {
        return side;
    }

    @Override
    public float getValue() {
        return getSide();
    }
}
