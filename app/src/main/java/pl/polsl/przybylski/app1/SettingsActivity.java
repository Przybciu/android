package pl.polsl.przybylski.app1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_MESSAGE_MIN = "EXTRA_MESSAGE_MIN";
    public static final String EXTRA_MESSAGE_MAX = "EXTRA_MESSAGE_MAX";
    EditText editText;
    EditText editMin;
    EditText editMax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        editText = findViewById(R.id.editTextValue);
        editMin = findViewById(R.id.editTriangleAreaSum);
        editMax = findViewById(R.id.editSquareAreaSum);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String messageMin = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_MIN);
        String messageMax = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_MAX);
        editText.setText(message);
        editMin.setText(messageMin);
        editMax.setText(messageMax);
    }

    public void openMainActivity(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        String message = String.valueOf(editText.getText());
        Integer min = Integer.valueOf(String.valueOf(editMin.getText()));
        Integer max = Integer.valueOf(String.valueOf(editMax.getText()));
        if (min >= max) {
            Toast toast = Toast.makeText(getApplicationContext(), "max musi być większy od min!", Toast.LENGTH_LONG);
            toast.show();
        } else {
            String messageMin = min.toString();
            String messageMax = max.toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            intent.putExtra(EXTRA_MESSAGE_MIN, messageMin);
            intent.putExtra(EXTRA_MESSAGE_MAX, messageMax);
            startActivity(intent);
        }
    }
}