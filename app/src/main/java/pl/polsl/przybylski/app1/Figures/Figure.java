package pl.polsl.przybylski.app1.Figures;

import java.text.DecimalFormat;

public abstract class Figure {
    protected final DecimalFormat format = new DecimalFormat("#.###");
    protected String name;

    public Figure(String name) {
        this.name = name;
    }

    public abstract float getSurfaceArea();

    public String getBasicFigureInfo() {
        return name + " having surface area = " + format.format(getSurfaceArea());
    }

    public abstract void printFigureInfo();

    public String getName() {
        return name;
    }

    public abstract String getFeatureInfo();

    public abstract float getFeature();

    public String getSurfaceAreaFormatted() {
        String temp = format.format(getSurfaceArea());
        if (temp.length() == 1) {
            temp += '.';
        }
        while (temp.length() < 5) {
            temp += '0';
        }
        return temp;
    }

    public abstract int getDrawable();

    public abstract float getValue();
}
