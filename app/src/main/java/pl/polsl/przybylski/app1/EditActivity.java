package pl.polsl.przybylski.app1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import pl.polsl.przybylski.app1.Figures.Figure;

public class EditActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE_VALUE = "EXTRA_MESSAGE_VALUE";
    public static final String EXTRA_MESSAGE_TYPE = "EXTRA_MESSAGE_TYPE";
    private EditText editTextValue;
    private Spinner spinner;
    private String type;
    public static List<Figure> figures;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        editTextValue = findViewById(R.id.editTextValue);
        spinner = findViewById(R.id.spinner);
        figures = MainActivity.figureList;
        Intent intent = getIntent();
        String messageType = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_TYPE);
        String messageValue = intent.getStringExtra(MainActivity.EXTRA_MESSAGE_VALUE);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Kwadrat");
        arrayList.add("Trójkąt");
        arrayList.add("Koło");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if(messageType != null && !messageType.isEmpty() && messageValue != null && !messageValue.isEmpty()) {
            editTextValue.setText(messageValue);
            int spinnerPosition = arrayAdapter.getPosition(messageType);
            spinner.setSelection(spinnerPosition);
        }
    }

    public void openMainActivity(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        String message = String.valueOf(editTextValue.getText());
        if(message.isEmpty()) {
            Toast toast = Toast.makeText(getApplicationContext(), "wypełnij pole!", Toast.LENGTH_LONG);
            toast.show();
        } else {
            intent.putExtra(EXTRA_MESSAGE_TYPE, type);
            intent.putExtra(EXTRA_MESSAGE_VALUE, message);
            startActivity(intent);
        }
    }
}